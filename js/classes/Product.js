class Product {
	constructor(id, title, desc, specs, price, stock, active, image) {
		this.id = id;
		this.title = title;
		this.desc = desc;
		this.specs = specs;
		this.price = price;
		this.stock = stock;
		this.active = active;
		this.image = image;
	}
}

export default Product;
