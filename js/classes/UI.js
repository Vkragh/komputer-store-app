import handleImage from "../util/handleImage.js";

class UI {
	constructor() {
		this.productDescNode = document.querySelector("#product-desc");
		this.productTitleNode = document.querySelector("#product-title");
		this.productDropdown = document.querySelector(".products");
		this.loanContainer = document.querySelector("#loanContainer");
		this.loanAmountNode = document.querySelector("#loan-amount");
		this.repayContainer = document.querySelector("#repayContainer");
		this.payAmountContainer = document.querySelector("#pay-amount");
		this.balanceAmountNode = document.querySelector("#balance-amount");
		this.computerContainer = document.querySelector("#computer-container");
		this.productsContainer = document.querySelector(".purchased-products");
		this.productCounter = document.querySelector("#product-counter");
		this.loanBtn = document.querySelector("#loan-btn");
		this.bankBtn = document.querySelector("#bank-btn");
		this.workBtn = document.querySelector("#work-btn");
		this.repayBtn = document.querySelector("#repay-btn");
	}
}

const hideContainer = container => {
	container.classList.add("d-none");
};

const showContainer = container => {
	container.classList.remove("d-none");
};

const setText = (el, str) => {
	el.innerText = str;
};

const renderProductContainer = (product, image) => {
	const imageDiv = document.createElement("div");
	imageDiv.append(image);
	imageDiv.classList.add(
		"d-flex",
		"col-lg-4",
		"justify-content-center",
		"align-items-center"
	);

	const descDiv = document.createElement("div");
	descDiv.classList.add(
		"d-flex",
		"col-lg-4",
		"justify-content-center",
		"align-items-center"
	);
	const laptopDiv = document.createElement("div");
	laptopDiv.classList.add(
		"laptop-info",
		"d-flex",
		"justify-content-center",
		"align-items-center",
		"flex-column",
		"text-center"
	);
	const h3 = document.createElement("h3");
	h3.innerText = product.title;
	const p = document.createElement("p");
	p.innerText = product.desc;
	laptopDiv.append(h3, p);
	descDiv.appendChild(laptopDiv);

	const priceDiv = document.createElement("div");
	priceDiv.classList.add(
		"col-lg-4",
		"d-flex",
		"justify-content-center",
		"align-items-center",
		"flex-column"
	);
	const h4 = document.createElement("h4");
	const btn = document.createElement("button");
	h4.classList.add("my-3");
	h4.innerText = product.price;
	btn.classList.add("btn", "btn-success", "purchase-btn");
	btn.dataset.price = product.price;
	btn.dataset.id = product.id;
	btn.innerText = "BUY NOW!!";
	priceDiv.append(h4, btn);

	return [imageDiv, descDiv, priceDiv];
};

const updatePurchasedContainer = boughtProducts => {
	ui.productsContainer.innerHTML = "";
	ui.productCounter.innerText = boughtProducts.length;

	boughtProducts.forEach(laptop => {
		const listItem = document.createElement("li");
		const h4 = document.createElement("h4");
		const p = document.createElement("p");
		const ul = document.createElement("ul");
		const div = document.createElement("div");
		const img = handleImage(laptop.image);
		const imgDiv = document.createElement("div");

		div.classList.add("col-lg-6");
		listItem.classList.add(
			"list-group-item",
			"p-5",
			"d-flex",
			"justify-content-between"
		);
		h4.innerText = laptop.title;
		p.classList.add("mt-2");
		p.innerText = laptop.desc;

		img.classList.add("product-image");
		imgDiv.classList.add("col-lg-6", "d-flex", "justify-content-center");
		imgDiv.appendChild(img);

		laptop.specs.forEach(spec => {
			const li = document.createElement("li");
			li.innerText = spec;
			ul.appendChild(li);
		});

		div.append(h4, p, ul);
		listItem.append(div, imgDiv);
		ui.productsContainer.append(listItem);
	});
};

const ui = new UI();

export {
	hideContainer,
	showContainer,
	setText,
	renderProductContainer,
	updatePurchasedContainer,
};
export default ui;
