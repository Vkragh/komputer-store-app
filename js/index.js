import fetchData from './util/fetch.js';
import handleImage from './util/handleImage.js';
import Product from './classes/Product.js';
import ui, {
	hideContainer,
	showContainer,
	setText,
	renderProductContainer,
	updatePurchasedContainer,
} from './classes/UI.js';

let bankBalance = 0;
let bankLoans = 0;
let totalLoans = 0;
let loanAmount = 0;
let payAmount = 0;

const products = [];
const boughtProducts = [];

ui.loanBtn.addEventListener('click', () => {
	loanHandler();
});

ui.bankBtn.addEventListener('click', () => {
	bankHandler();
});

ui.workBtn.addEventListener('click', () => {
	workHandler();
});

ui.repayBtn.addEventListener('click', () => {
	repayHandler();
});

fetchData('https://noroff-komputer-store-api.herokuapp.com/computers').then(
	data => {
		data.forEach(laptop => {
			const product = new Product(
				laptop.id,
				laptop.title,
				laptop.description,
				laptop.specs,
				laptop.price,
				laptop.stock,
				laptop.active,
				laptop.image
			);
			products.push(product);
			ui.productDropdown.innerHTML += `<option value="${laptop.id}">${laptop.title}</option>`;
		});
	}
);

const repayHandler = () => {
	if (payAmount < loanAmount)
		return alert(
			'You dont have sufficient funds to repay your loan at this moment'
		);

	const deductionAmount = payAmount - loanAmount;
	payAmount = 0;
	loanAmount = 0;
	bankBalance += deductionAmount;
	bankLoans--;

	setText(ui.payAmountContainer, payAmount);
	setText(ui.balanceAmountNode, bankBalance);
	setText(ui.loanAmountNode, loanAmount);
	hideContainer(ui.repayContainer);
	hideContainer(ui.loanContainer);
};

const workHandler = () => {
	payAmount += 100;
	setText(ui.payAmountContainer, payAmount);
};

const bankHandler = () => {
	if (bankLoans < 1) {
		bankBalance += payAmount;
	} else {
		const deductionAmount = (payAmount / 100) * 10;
		const leftOverAmount = loanAmount - deductionAmount;
		if (leftOverAmount <= 0) {
			bankBalance += Math.abs(leftOverAmount);
			loanAmount = 0;
			bankLoans--;
			hideContainer(ui.loanContainer);
			hideContainer(ui.repayContainer);
		} else {
			loanAmount -= deductionAmount;
		}

		bankBalance += payAmount - deductionAmount;
		setText(ui.loanAmountNode, loanAmount);
	}
	payAmount = 0;
	setText(ui.payAmountContainer, payAmount);
	setText(ui.balanceAmountNode, bankBalance);
};

const loanHandler = () => {
	if (!bankLoans < 1) return alert('You already have an unpaid loan');

	let loan = prompt('Enter loan amount');

	if (loan === null) return;
	if (!Number(loan)) return alert('Needs to be a number');
	if (boughtProducts.length === 0 && totalLoans >= 1)
		return alert('You need to buy a computer before taking a new loan');
	if (bankBalance < 100)
		return alert('You need to have atleast 100kr in your balance');
	if (!Number(loan / 2 <= bankBalance))
		return alert('you can maximum loan 2 times the amount of your balance');

	loan = Number(loan);
	setText(ui.loanAmountNode, loan);
	bankLoans++;
	totalLoans++;
	loanAmount = loan;
	bankBalance += loan;
	setText(ui.balanceAmountNode, bankBalance);
	showContainer(ui.loanContainer);
	showContainer(ui.repayContainer);
};

const purchaseHandler = (price, id) => {
	if (price > bankBalance)
		return alert('You dont have sufficient funds to purchase this');

	bankBalance -= price;
	setText(ui.balanceAmountNode, bankBalance);
	alert("Congratulations, you've just bought a new laptop");

	const boughtProduct = products.find(product => Number(id) === product.id);
	boughtProducts.push(boughtProduct);
	updatePurchasedContainer(boughtProducts);
};

ui.productDropdown.addEventListener('change', e => {
	const id = e.target.value;
	products.forEach(product => {
		if (product.id == id) {
			setText(ui.productDescNode, product.desc);
			setText(ui.productTitleNode, product.title);
			const productImage = handleImage(product.image);
			ui.computerContainer.innerHTML = '';
			const elements = renderProductContainer(product, productImage);
			elements.forEach(el => {
				ui.computerContainer.appendChild(el);
			});
		}
	});
});

ui.computerContainer.addEventListener('click', e => {
	if (e.target.classList.contains('purchase-btn')) {
		const productPrice = e.target.dataset.price;
		const productId = e.target.dataset.id;
		purchaseHandler(productPrice, productId);
	}
});
