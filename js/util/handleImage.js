const handleImage = image => {
	let img = document.createElement("img");
	img.src = `https://noroff-komputer-store-api.herokuapp.com/${image}`;

	img.onerror = () => {
		img.src =
			"https://gitlab.com/Vkragh/komputer-store-app/-/raw/master/images/404-error.jpg";
	};

	return img;
};

export default handleImage;
