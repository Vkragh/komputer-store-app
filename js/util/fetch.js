const fetchData = async url => {
	try {
		const json = await fetch(url);
		const data = await json.json();

		return data;
	} catch (error) {
		console.log(error);
	}
};

export default fetchData;
